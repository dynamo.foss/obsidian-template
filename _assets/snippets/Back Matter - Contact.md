> [!Mối quan hệ]
> 

## 👨‍💼 Định danh
- Tên đầy đủ: `= this.person.fullname`
- Biệt danh: `= this.person.nickname`
- Giới tính: `= choice(this.person.gender > 1, "Khác", choice(this.person.gender != 1, "Nam", "Nữ"))`

## 📞 Liên lạc
- Điện thoại: `= this.contact.phone`
- Email: `= this.contact.mail`

## 📜 Tham chiếu
- Địa chỉ:: 
- Công ty:: 
<%*
let sensitive_select = await tp.system.suggester((item) => item, ["Không có", "Có thông tin PII"])
if (sensitive_select != "Không có") {
_%>


## 🚑 An sinh
- Nhóm máu:: 
- Ngày sinh:: 
<%* } _%>

## 💭 Suy nghĩ
<!-- Liệt kê các ghi chú hoặc suy nghĩ về mối quan hệ này -->