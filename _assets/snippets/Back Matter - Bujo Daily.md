## 🔷 Theo dõi lịch trình

---
## 🎯 Mục tiêu
<!-- Mục tiêu cần đạt trong suốt cả ngày, được chuẩn bị từ đầu ngày và hạn chế bổ sung đến hết ngày -->

---
## 🚀 Việc còn tồn đọng
<!-- Công việc còn dang dở mà đã được lên kế hoạch nhưng vào cuối ngày không thể hoàn thành -->

---
## 📕 Nhắc nhở
<!-- Nhắc nhở cho chính bản thân -->

---
## 📚 Học 1 sàng khôn / TIL
<!-- Liệt kê những ghi chú, những thông tin được học, đọc hoặc biết tới -->

---
## 💪 Sức khỏe
<!-- Liệt kê các hoạt động về sức khỏe thể chất và sức khỏe tinh thần -->

---
## 💵 Tài chính
<!-- Liệt kê các hoạt động chi tiêu và tài chính cá nhân/gia đình -->

---
## 💬 Cảm xúc, Quan sát và Suy nghĩ
### 😀 Cảm xúc
<!-- Miêu tả sơ qua về cảm xúc chủ đạo của ngày, cảm xúc của từng buổi hoặc thời điểm thay đổi cảm xúc -->

### 👀 Quan sát
<!-- Miêu tả sơ qua về cảm xúc chủ đạo của ngày, cảm xúc của từng buổi hoặc thời điểm thay đổi cảm xúc -->

### 💭 Suy nghĩ
<!-- Liệt kê các ghi chú hoặc suy nghĩ được tổng kết cuối ngày -->

---
## 📝 Các ghi chú

### Ghi chú được tạo thêm
```dataview
List FROM "" WHERE dateformat(created, "YYYYMMdd") = dateformat(date(this.file.name), "YYYYMMdd") SORT created asc
```

### Ghi chú được chỉnh sửa
```dataview
List FROM "" WHERE dateformat(updated, "YYYYMMdd") = dateformat(date(this.file.name), "YYYYMMdd") SORT updated asc
```