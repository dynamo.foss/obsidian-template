> [!Định nghĩa]
> <% tp.file.cursor(1) %>

## 🛈 Nguồn thông tin
<!-- Phải luôn luôn có 1 nguồn thông tin -->
- 
## 🔗 Tham chiếu tới
<!-- Dẫn link tới trang không có trong định nghĩa mà có liên quan -->
- 
## 🚀 Nhiệm vụ
<!-- Liệt kê các nhiệm vụ cần làm liên quan đến ghi chú này -->
- 
## 💭 Suy nghĩ
<!-- Liệt kê những suy nghĩ trong đầu mà còn đắn đo -->
- 