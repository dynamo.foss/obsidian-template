# Tags
## Tags - Theme
- theme/{{theme}}
## Tags - Index
- index/{{moc}}
## Tags - Bujo
- type/structure
- structure/bujo
## Tags - Person - Contact
- type/person
- person/contact
## Tags - Kanban
- type/kanban
- kanban/{{type of task}}
- status/{{status of task}}
- source/{{source of task}}
## Tags - Book
- type/book
## Tags - Chart
- type/chart
- chart/{{type of chart}}
## Tags - Company
- type/company
## Tags - Concept
- type/concept
## Tags - CodeSnippet
- type/snippets
## Tags - Insight
- type/insight
## Tags - Meeting
- type/meeting
- meeting/{{kind of meeting: 1vs1, group, seminar, event...}}
## Tags - Question
- type/question
## Tags - Sketchnote
- type/sketchnote
## Tags - Structure - MOC
- type/structure
- structure/moc
## Tags - Term
- type/term
## Tags - Tool
- type/tool
## Tags - Place
- type/place
# UID
uid: <% tp.file.creation_date("YYYYMMDDHHmmss") %>
# Data
## Data - Person
  nickname:
    - <% tp.file.cursor(1) %>
  <%*
  let gender_select = await tp.system.suggester((item) => item, ["Nam", "Nữ", "Khác"])
  let gender = 2
  switch (gender_select) {
    case "Nam": gender = 0; break
    case "Nữ": gender = 1; break
  }
  _%>
  gender: <% gender %>
## Data - Person - Contact
contact:
  phone:
    - 
  mail: