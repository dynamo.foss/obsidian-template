<%*
let title = tp.file.title;
if (title == "Untitled") {
  title = await tp.system.prompt("Title");
  await tp.file.rename(title);
}
let fullname = await tp.system.prompt("Tên đầy đủ")
-%>
<% "---" %>
aliases:
  - <% title %>
  - <% fullname %>
tags:
  <% tp.file.include("[[Front Matter#Tags - Person - Contact]]") %>
<% tp.file.include("[[Front Matter#UID]]") %>
person:
  fullname: <% fullname %>
  <% tp.file.include("[[Front Matter#Data - Person]]") %>
<% tp.file.include("[[Front Matter#Data - Person - Contact]]") %>
template: "Person - Contact"
<% "---" %>

# <% title %>
<% tp.file.include("[[Back Matter - Contact]]") %>