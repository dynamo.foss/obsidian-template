<%*
let title = tp.file.title;
if (title == "Untitled") {
  title = await tp.system.prompt("Title");
  await tp.file.rename(title);
}
-%>
<% "---" %>
aliases:
  - <% title %>
tags:
  <% tp.file.include("[[Front Matter#Tags - Structure - MOC]]") %>
<% tp.file.include("[[Front Matter#UID]]") %>
cssclasses:
  - wide-page
template: "Map of Content"
<% "---" %>

# <% title %>

```dataview
<% tp.file.cursor(3) %>
```