<%*
let title = tp.file.title;
if (title == "Untitled") {
  title = await tp.system.prompt("Title");
  await tp.file.rename(title);
}
-%>
<% "---" %>
aliases:
  - <% title %>
tags:
<% tp.file.include("[[Front Matter#UID]]") %>
template: "Default"
<% "---" %>

# <% title %>

<% tp.file.cursor(3) %>