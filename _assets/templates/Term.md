<%*
let title = tp.file.title;
if (title == "Untitled") {
  title = await tp.system.prompt("Title");
  await tp.file.rename(title);
}
-%>
<% "---" %>
aliases:
  - <% title %>
tags:
  <% tp.file.include("[[Front Matter#Tags - Term]]") %>
<% tp.file.include("[[Front Matter#UID]]") %>
template: "Term"
<% "---" %>

# <% title %>
<% tp.file.include("[[Back Matter - Term]]") %>