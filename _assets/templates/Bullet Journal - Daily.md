<%*
let title = moment(tp.file.title,'YYYY-MM-DD').format("dddd, MMMM DD, YYYY")
-%>
<% "---" %>
aliases:
- 
tags:
  <% tp.file.include("[[Front Matter#Tags - Bujo]]") %>
<% tp.file.include("[[Front Matter#UID]]") %>
template: "Bullet Journal - Daily"
<% "---" %>

# <% title %>

<< [[<% fileDate = moment(title).subtract(1, 'd').format('YYYY-MM-DD') %>|Hôm qua]] | [[<% fileDate = moment(title).add(1, 'd').format('YYYY-MM-DD') %>|Ngày mai]] >>

---
<% tp.file.include("[[Back Matter - Bujo Daily]]") %>